package rest;

import org.springframework.stereotype.Repository;
import rest.entity.*;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FoodDao {
    List<Food> foodList;

    public FoodDao() {
        foodList = new ArrayList<Food>() ;
        foodList.add( Food.builder().id(1L).name("Cake").price(50).build());
        foodList.add( Food.builder().id(2L).name("Chicken").price(60).build());
    }
    public List<Food> getAllFood() {
        return this.foodList;
    }

    public Food getFood(Long id) {
        return this.foodList.get(Math.toIntExact(id-1));
    }

    public Food saveFood(Food food) {
       food.setId((long) this.foodList.size());
        this.foodList.add(food);
        return food;
    }
}
