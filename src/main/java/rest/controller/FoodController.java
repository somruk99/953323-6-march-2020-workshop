package rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import rest.service.FoodService;
import rest.entity.*;

@Controller
public class FoodController {

    @Autowired
    FoodService foodService;
    @GetMapping("/products")
    public ResponseEntity getAllFood() {
        return ResponseEntity.ok(this.foodService.getAllFood());
    }

    @GetMapping("/products/{id}")
    public ResponseEntity getFoodById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.foodService.getFood(id));
    }

    @PostMapping("/products")
    public ResponseEntity saveFood(@RequestBody Food food) {
        return ResponseEntity.ok(this.foodService.saveFood(food));
    }
}
