package rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.FoodDao;
import rest.entity.*;

import java.util.List;

@Service
public class FoodService {
    @Autowired
    FoodDao foodDao;
    public List<Food> getAllFood() {
        return this.foodDao.getAllFood();
    }
    public Food getFood(Long id) {
        return this.foodDao.getFood(id);
    }

    public Food saveFood(Food food) {
        return this.foodDao.saveFood(food);
    }
}
