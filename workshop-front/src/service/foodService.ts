import { Injectable } from '@angular/core';
import Food from 'src/app/entity/food';


@Injectable({
  providedIn: 'root'
})
export class foodService {
  private foods:Food[];
  constructor() { 
    this.foods = [
    { id: 0o1, name: 'name 1', price: 100 },
    { id: 0o2, name: 'name 2', price: 200 },
    { id: 0o3, name: 'name 3', price: 300 }
];
} 
  findAll(): Food[] {
    return this.foods;
}
find(id: number): Food {
  return this.foods[this.getSelectedIndex(id)];
}

private getSelectedIndex(id: number) {
  for (var i = 0; i < this.foods.length; i++) {
      if (this.foods[i].id == id) {
          return i;
      }
  }
  return -1;
}
}
