import { TestBed } from '@angular/core/testing';

import { foodService } from './foodService';

describe('foodService', () => {
  let service: foodService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(foodService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
