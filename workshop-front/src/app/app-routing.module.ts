import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodComponent } from './food/food.component';
import { OrderComponent } from './order/order.component';


const routes: Routes = [
  { path: '', component: FoodComponent },
	{ path: 'foods', component: FoodComponent },
	{ path: 'order', component: OrderComponent },
	{ path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
