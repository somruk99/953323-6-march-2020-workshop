import { Component, OnInit } from '@angular/core';
import Food from '../entity/food';
import { foodService } from 'src/service/foodService';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {
  foods: Food[];
  constructor( private service :foodService) { 
  
  }

  ngOnInit(): void {
    this.foods = this.service.findAll();
  }

}
