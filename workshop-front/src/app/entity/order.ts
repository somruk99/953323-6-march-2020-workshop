import Food from './food';

export default class Order {
  //id: number;
  //tableId: number;
  food: Food;
  foodAmount: number;
}
