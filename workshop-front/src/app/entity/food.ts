export default class Food {
  id: number;
  name: string;
  price: number;
}
